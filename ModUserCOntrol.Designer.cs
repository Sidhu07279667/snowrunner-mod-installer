﻿namespace SnowRunnerModIo
{
    partial class ModUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModUserControl));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.radioButtonIsInstalled = new System.Windows.Forms.RadioButton();
            this.buttonInstall = new System.Windows.Forms.Button();
            this.linkLabelURL = new System.Windows.Forms.LinkLabel();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.buttonDisable = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // textBoxName
            // 
            resources.ApplyResources(this.textBoxName, "textBoxName");
            this.textBoxName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            // 
            // radioButtonIsInstalled
            // 
            resources.ApplyResources(this.radioButtonIsInstalled, "radioButtonIsInstalled");
            this.radioButtonIsInstalled.Name = "radioButtonIsInstalled";
            this.radioButtonIsInstalled.TabStop = true;
            this.radioButtonIsInstalled.UseVisualStyleBackColor = true;
            // 
            // buttonInstall
            // 
            resources.ApplyResources(this.buttonInstall, "buttonInstall");
            this.buttonInstall.Name = "buttonInstall";
            this.buttonInstall.UseVisualStyleBackColor = true;
            this.buttonInstall.Click += new System.EventHandler(this.button1_Click);
            // 
            // linkLabelURL
            // 
            resources.ApplyResources(this.linkLabelURL, "linkLabelURL");
            this.linkLabelURL.Name = "linkLabelURL";
            this.linkLabelURL.TabStop = true;
            this.linkLabelURL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelURL_LinkClicked);
            // 
            // textBoxId
            // 
            resources.ApplyResources(this.textBoxId, "textBoxId");
            this.textBoxId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            // 
            // buttonDisable
            // 
            resources.ApplyResources(this.buttonDisable, "buttonDisable");
            this.buttonDisable.Name = "buttonDisable";
            this.buttonDisable.UseVisualStyleBackColor = true;
            this.buttonDisable.Click += new System.EventHandler(this.buttonDisable_Click);
            // 
            // buttonRemove
            // 
            resources.ApplyResources(this.buttonRemove, "buttonRemove");
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // ModUserControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonDisable);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.linkLabelURL);
            this.Controls.Add(this.buttonInstall);
            this.Controls.Add(this.radioButtonIsInstalled);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ModUserControl";
            this.Load += new System.EventHandler(this.ModUserControl_Load);
            this.BackColorChanged += new System.EventHandler(this.ModUserControl_BackColorChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox textBoxName;
        public System.Windows.Forms.RadioButton radioButtonIsInstalled;
        private System.Windows.Forms.Button buttonInstall;
        private System.Windows.Forms.LinkLabel linkLabelURL;
        public System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Button buttonDisable;
        private System.Windows.Forms.Button buttonRemove;
    }
}
