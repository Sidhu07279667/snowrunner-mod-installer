﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowRunnerModIo
{
    public class SMod
    {
        public int Id { get; set; }
        public string Folder { get; set; }
        public string PakFilename { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsPakInstalled { get; set; }
    }

}
