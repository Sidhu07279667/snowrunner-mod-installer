﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    public partial class InstallSubscribedMods : Form
    {
        //public static IMySettings Settings
        //{
        //    get
        //    {
        //        return settings;
        //    }
        //}

        //private static string ss = Path.Combine(Application.StartupPath, "settings.json");

        //private static IMySettings settings = new ConfigurationBuilder<IMySettings>().UseJsonFile(ss).Build();

        private void showOptions()
        {
            var cf = new ConfigForm();
            cf.ShowDialog();
        }

        public InstallSubscribedMods()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            if (string.IsNullOrWhiteSpace(Program.Settings.APIKey) || string.IsNullOrWhiteSpace(Program.Settings.Token) || Program.Settings.TokenExpire < DateTime.Now)
            {
                showOptions();
            }

            //Console.WriteLine(Path.Combine(Application.StartupPath, "settings.json"));
            //Console.WriteLine(settings);
            //var cf = new ConfigForm();
            //cf.ShowDialog();

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            backgroundWorker2.RunWorkerAsync();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showOptions();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButtonRepairFiles_Click(object sender, EventArgs e)
        {
            ModIoREST.Instance.RepairInstalledMods();
            toolStripButtonShowSubscribed_Click(sender, e);
        }

        private Color modsBackColor = SystemColors.Control;
        private Color modsAltBackColor = ControlPaint.Light(SystemColors.Control, 0.009f);

        private void toolStripButtonShowSubscribed_Click(object sender, EventArgs e)
        {
            UpdateSubscribed();
        }

        private void UpdateSubscribed()
        {
            Color bg = modsBackColor;

            var mods = ModIoREST.Instance.GetSubscribedMods();

            this.Invoke(new Action(() =>
            {
                panelMods.Controls.Clear();
                //for (int i = 0; i < 20; i++)
                foreach (var item in mods)
                {
                    var muc = new ModUserControl();
                    muc.Mod = item;
                    muc.Dock = DockStyle.Top;
                    muc.BackColor = bg;
                    if (bg == modsBackColor)
                        bg = modsAltBackColor;
                    else
                        bg = modsBackColor;
                    panelMods.Controls.Add(muc);
                }
            }));
        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var settpath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(ModIoREST)).Location);
            var spath = Path.Combine(settpath, "snowrunnermodinstaller.pdf");
            try
            {
                Process.Start(spath);
            }
            catch { }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutForm().ShowDialog();
        }

        #region InstallAll

        private void toolStripButtonInstallAll_Click(object sender, EventArgs e)
        {
            UpdateSubscribed();

            this.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var allMods = ModIoREST.Instance.GetSubscribedMods();
            int i = 0;

            foreach (var mod in allMods)
            {
                i++;
                toolStripStatusLabelInstallAllProgress.Text = string.Format(toolStripStatusLabelInstallingModPleaseWait.Text, mod.name);
                ModIoREST.Instance.InstallMod(mod);
                backgroundWorker1.ReportProgress((int)(100f * i / allMods.Count));
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            toolStripProgressBarInstallAll.Value = e.ProgressPercentage;
            Console.WriteLine(e.ProgressPercentage);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabelInstallAllProgress.Text = string.Format(toolStripStatusLabelInstallAllCompleted.Text);
            MessageBox.Show(toolStripStatusLabelInstallAllCompleted.Text);
            toolStripProgressBarInstallAll.Value = 0;
            toolStripStatusLabelInstallAllProgress.Text = "";
            this.Enabled = true;
        }

        #endregion InstallAll

        private void toolStripButtonAddStandaloneMod_Click(object sender, EventArgs e)
        {
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void backgroundWorker2_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            UpdateSubscribed();
        }
    }
};