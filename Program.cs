﻿using Config.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    static class Program
    {
        public static IMySettings Settings = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            InitSettings();

            //Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("ru");
            //Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("uk");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ChooseMode());
        }


        public static void InitSettings()
        {
            var settpath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(ModIoREST)).Location);
            var spath = Path.Combine(settpath, "settings.json");
            Settings = new ConfigurationBuilder<IMySettings>().UseJsonFile(spath).Build();

        }
    }
}
